import Index from 'modules/home/Index.vue';

// Pacientes
import ListingPatients from 'modules/patients/ListingPatients.vue';
import FormPatients from 'modules/patients/FormPatients.vue';
// Alunos
import ListingStudent from 'modules/students/ListingStudent.vue';
import FormStudent from 'modules/students/FormStudent.vue';
// Lista de espera
import WaitingList from 'modules/waiting-list/Index.vue';
// Áreas de Estágio
import ListingStagingArea from 'modules/stagingArea/ListingStagingArea.vue';
import FormAreaStaging from 'modules/stagingArea/FormAreaStaging.vue';
// Quesitos
import ListingQuesits from 'modules/quesits/ListingQuesits.vue';
import FormQuesits from 'modules/quesits/FormQuesits.vue';
// Usuários
import UsersIndex from 'modules/users/Index.vue';
import UsersAdd from 'modules/users/Add.vue';
// Login
import Login from 'modules/login/Login.vue';
// Sessions
import ImportSessions from 'modules/import-sessions/Index.vue';
// Reports
import ReportSessionsAtended from 'modules/reports/session-atended/Index.vue';
import ReportSessionsAtendedByStudent from 'modules/reports/session-atended-by-student/Index.vue';
import ReportPatients from 'modules/reports/patients/Index.vue';
import ReportEvaluationStudent from 'modules/reports/evaluation-students/Index.vue';

// App
import App from './App';

export default {
  '/login': {
    name: 'login',
    component: Login,
  },
  '/': {
    component: App,
    subRoutes: {
      '/': {
        name: 'dashboard',
        component: Index,
      },
      '/pacientes': {
        component: ListingPatients,
        name: 'pacientes',
      },
      '/pacientes/novo': {
        component: FormPatients,
        name: 'pacientes-novo',
        title: 'Cadastrar Paciente',
      },
      '/pacientes/:id/editar': {
        component: FormPatients,
        name: 'pacientes-editar',
        title: 'Editar Paciente',
      },
      '/alunos': {
        component: ListingStudent,
        name: 'alunos',
      },
      '/alunos/novo': {
        component: FormStudent,
        name: 'alunos-novo',
        title: 'Novo Aluno',
      },
      '/alunos/:id/editar': {
        component: FormStudent,
        name: 'alunos-editar',
        title: 'Editar Aluno',
      },
      'lista-de-espera': {
        component: WaitingList,
        name: 'lista-de-espera',
      },
      '/especialidade': {
        component: ListingStagingArea,
        name: 'especialidade',
      },
      '/especialidade/novo': {
        component: FormAreaStaging,
        name: 'especialidade-novo',
        title: 'Cadastrar Especialidade',
      },
      '/especialidade/:id/editar': {
        component: FormAreaStaging,
        name: 'especialidade-editar',
        title: 'Editar Especialidade',
      },
      '/quesitos': {
        component: ListingQuesits,
        name: 'quesitos',
      },
      '/quesitos/novo': {
        component: FormQuesits,
        name: 'quesitos-novo',
        title: 'Novo Quesito',
      },
      '/quesitos/:id/editar': {
        component: FormQuesits,
        name: 'quesitos-editar',
        title: 'Editar Quesito',
      },
      '/usuarios': {
        component: UsersIndex,
        name: 'usuarios',
      },
      '/usuarios/:id/editar': {
        component: UsersAdd,
        name: 'usuarios-editar',
        title: 'Editar Usuário',
      },
      '/usuarios/novo': {
        component: UsersAdd,
        name: 'usuarios-novo',
        title: 'Novo Usuário',
      },
      '/importar-sessoes-atendidas': {
        component: ImportSessions,
        title: 'Importar Sessões Atendidas',
        name: 'importar-sessoes-atendidas',
      },
      '/relatorio-sessoes-atendidas': {
        component: ReportSessionsAtended,
        title: 'Relatório de Sessões Atendidas',
        name: 'relatorio-sessoes-atendidas',
      },
      '/relatorio-sessoes-atendidas-por-aluno': {
        component: ReportSessionsAtendedByStudent,
        title: 'Relatório de Sessões Atendidas Por Aluno',
        name: 'relatorio-sessoes-atendidas-por-aluno',
      },
      '/relatorio-de-pacientes': {
        component: ReportPatients,
        title: 'Relatório de Pacientes',
        name: 'relatorio-pacients',
      },
      '/relatorio-avaliativo-de-alunos': {
        component: ReportEvaluationStudent,
        title: 'Relatório avaliativo de alunos',
        name: 'relatorio-avaliativo-de-aluno',
      },
    },
  },
};
