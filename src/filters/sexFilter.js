const sexData = {
  M: 'Masculino',
  F: 'Feminino',
  O: 'Outro',
};

export default (value) => sexData[value];
