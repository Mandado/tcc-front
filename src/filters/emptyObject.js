import is from 'is_js';

export default (object) => {
  const isEmpty = (property) => property !== '__ob__' && is.empty(object[property]);
  const properties = Object.getOwnPropertyNames(object).map(isEmpty);
  return properties.length > 0;
};
