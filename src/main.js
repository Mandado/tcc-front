import Vue from 'vue';
import store from './store';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import VueValidator from 'vue-validator';
import VueMoment from 'vue-moment';
import routes from './routes';
import sexFilter from './filters/sexFilter';
import emptyObject from './filters/emptyObject';
import LoginRepository from 'modules/login/services/repository';
import { sync } from 'vuex-router-sync';
import { number, cpf, positive, email, isDate, isDateBr } from './validators';
import { errorAlert } from './services/alert';
import buildMasks from './masks';
import _ from 'lodash';

// third packages
import 'sweetalert2/dist/sweetalert2.min.css';
import { setDataUserLogged } from 'modules/login/vuex/actions';

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueValidator);
Vue.use(VueMoment);

Vue.validator('number', number);
Vue.validator('cpf', cpf);
Vue.validator('positive', positive);
Vue.validator('email', email);
Vue.validator('date', isDate);
Vue.validator('dateBr', isDateBr);

Vue.filter('sex', sexFilter);
Vue.filter('emptyObject', emptyObject);

buildMasks();

const router = new VueRouter();

sync(store, router);

/* eslint-disable no-new */
const rootComponentApp = Vue.extend({
  store,
});

router.map(routes);

Vue.http.interceptors.push((request, next) => {
  const idToken = localStorage.getItem('id_token');

  if (idToken) {
    Vue.http.headers.common.Authorization = `Bearer ${idToken}`;
  } else {
    Vue.http.headers.common.Authorization = '';
  }

  next((response) => {
    if (response.status === 422) {
      const errors = response.json();

      const errorsFlat = _.flatten(_.values(errors));
      const error = errorsFlat.join('<br/> ');
      errorAlert(error);
    }

    if (response.status === 401) {
      router.go({ name: 'login' });
    }
  });
});
router.beforeEach(({ next, to }) => {
  const idToken = localStorage.getItem('id_token');

  if (to.path !== '/login' && !idToken) {
    LoginRepository.isLogged()
    .then(() => next())
    .catch(() => {
      localStorage.removeItem('id_token');
      router.go('/login');
    });
  } else {
    setDataUserLogged(store);

    next();
  }
});

router.start(rootComponentApp, '#app');
