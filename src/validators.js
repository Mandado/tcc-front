import is from 'is_js';

const regexDateBr = /([0-9]{2})\/([0-9]{2})\/([0-9]{4})/;

export const number = (val) => /[0-9]/.test(val);

export const positive = (val) => {
  const n = ~~Number(val);
  return String(n) === val && n >= 0;
};

export const email = (value) => is.email(value);

export const isDate = (value) => {
  if (!value) return false;
  const [day, month, year] = value.split('/');
  const newDateString = `${month}/${day}/${year}`;

  return is.dateString(newDateString);
};
export const isDateBr = (value) => regexDateBr.test(value);

export const cpf = (cpf) => {
  let sum = 0;
  let remainder = 0;

  cpf = cpf.replace('.', '')
  .replace('.', '')
  .replace('-', '')
  .trim();

  let allEqual = true;
  for (let i = 0; i < cpf.length - 1; i++) {
    if (cpf[i] !== cpf[i + 1]) {
      allEqual = false;
    }
  }
  if (allEqual) {
    return false;
  }

  for (let i = 1; i <= 9; i++) {
    const sub = parseInt(cpf.substring(i - 1, i), 10);

    sum = sum + sub * (11 - i);
    remainder = (sum * 10) % 11;
  }

  if ((remainder === 10) || (remainder === 11)) {
    remainder = 0;
  }
  if (remainder !== parseInt(cpf.substring(9, 10), 10)) {
    return false;
  }

  sum = 0;
  for (let i = 1; i <= 10; i++) {
    sum = sum + parseInt(cpf.substring(i - 1, i), 10) * (12 - i); remainder = (sum * 10) % 11;
  }

  if ((remainder === 10) || (remainder === 11)) {
    remainder = 0;
  }

  if (remainder !== parseInt(cpf.substring(10, 11), 10)) {
    return false;
  }

  return true;
};
