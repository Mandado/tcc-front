import izitoast from '../../node_modules/izitoast/dist/js/iziToast.min.js';
import swal from 'sweetalert2';

izitoast.settings({
  position: 'bottomCenter',
});

const configConfirm = {
  showCloseButton: true,
  showCancelButton: true,
  confirmButtonClass: 'btn btn-lg btn-success',
  cancelButtonClass: 'btn btn-lg btn-cancel',
  confirmButtonText: 'Sim',
  cancelButtonText: 'Não',
  buttonsStyling: true,
};
const messageError = 'Erro ao carregar dados';

const buildConfirm = (message) => ({
  ...configConfirm,
  text: message,
  title: 'Ops!',
});


export const errorLoadData = (message) => {
  const messageF = message || messageError;
  izitoast.error({ title: 'Ops!', message: messageF });
};
export const successSaveData = (message) => izitoast.success({ title: 'Aeho!', message });
export const errorAlert = (message) => izitoast.error({ title: 'Ops!', message });
export const confirmAlert = (message) => swal(buildConfirm(message));
export const errorSaveData = (message) => izitoast.error({ title: 'Ops!', message });
