import is from 'is_js';

export const clearString = (value) => {
  if (is.string(value)) {
    return value.replace(/\D/g, '');
  }

  return '';
};
