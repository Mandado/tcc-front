import vMasker from 'vanilla-masker';
import Vue from 'vue';
import is from 'is_js';

function formatTelphoneMask(value, withValue = false) {
  const valueMask = value.replace(/\D/g, '');
  const mask = valueMask.length > 10 ? '(99) 99999-9999' : '(99) 9999-9999';

  return (withValue) ? { mask, valueMask } : mask;
}

function formatCpfMask(value) {
  return !is.empty(value) && '999.999.999-99';
}

const maskTel = (mask, event, el) => {
  const c = el || event.target;
  const m = formatTelphoneMask(c.value, true);
  mask.unMask();
  mask.maskPattern(m.mask);
  c.value = vMasker.toPattern(m.valueMask, m.mask);
};

function maskCpf() {
  Vue.nextTick(() => {
    if (this.el.value) {
      this.el.value = vMasker.toPattern(this.el.value, '999.999.999-99');
    }
  });
  vMasker(this.el).unMask();
  vMasker(this.el).maskPattern('999.999.999-99');
}

function maskDate() {
  Vue.nextTick(() => {
    if (this.el.value) {
      this.el.value = vMasker.toPattern(this.el.value, '99/99/9999');
    }
  });
  vMasker(this.el).maskPattern('99/99/9999');
}

function maskTelphone() {
  const mask = vMasker(this.el);
  Vue.nextTick(() => {
    if (this.el.value) {
      maskTel(mask, undefined, this.el);
    }
  });
  this.el.addEventListener('input', (event) => maskTel(mask, event));
  this.el.addEventListener('blur', (event) => maskTel(mask, event));
}

function getValueMasked(mask, value) {
  const valueMask = String(value);
  return vMasker.toPattern(valueMask, mask(valueMask));
}

export const maskValueTelphone = (value) => getValueMasked(formatTelphoneMask, value);
export const maskValueCpf = (value) => getValueMasked(formatCpfMask, value);

export default () => {
  Vue.directive('mask-cpf', maskCpf);
  Vue.directive('mask-phone', maskTelphone);
  Vue.directive('mask-date', maskDate);
};
