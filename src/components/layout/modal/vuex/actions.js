export const showModal = ({ dispatch }, name) => {
  window.$(`#${name}`).modal('show');
};
export const hideModal = ({ dispatch }, name) => {
  window.$(`#${name}`).modal('hide');
};
