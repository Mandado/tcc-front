import Vue from 'vue';
import Vuex from 'vuex';
// components
import {
  state as modalState,
  mutations as modalMutations,
} from 'layout/modal/vuex/index';

// modules
import {
  state as stagingAreaState,
  mutations as stagingAreaMutations,
} from 'modules/stagingArea/vuex/index';

import {
  state as PatientsState,
  mutations as PatientsMutations,
} from 'modules/patients/vuex/index';

import {
  state as LoginState,
  mutations as LoginMutations,
} from 'modules/login/vuex/index';

import {
  state as StudentsState,
  mutations as StudentsMutations,
} from 'modules/students/vuex/index';

import {
  state as QuesitState,
  mutations as QuesitMutations,
} from 'modules/quesits/vuex/index';

import {
  state as UsersState,
  mutations as UsersMutations,
} from 'modules/users/vuex/index';

import {
  state as WaitingListState,
  mutations as WaitingListMutations,
} from 'modules/waiting-list/vuex/index';
// Make vue aware of Vuex

Vue.use(Vuex);

const state = {
  ...modalState,
  ...stagingAreaState,
  ...PatientsState,
  ...LoginState,
  ...StudentsState,
  ...QuesitState,
  ...UsersState,
  ...WaitingListState,
};

// Create an object storing various mutations. We will write the mutation
const mutations = {
  ...modalMutations,
  ...stagingAreaMutations,
  ...PatientsMutations,
  ...LoginMutations,
  ...StudentsMutations,
  ...QuesitMutations,
  ...UsersMutations,
  ...WaitingListMutations,
};

// Combine the initial state and the mutations to create a Vuex store.
// This store can be linked to our app.
export default new Vuex.Store({
  state,
  mutations,
  strict: true,
});
