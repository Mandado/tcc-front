import { successSaveData, errorLoadData } from 'src/services/alert';

import UserRepository, { MODULE_API } from 'modules/users/services/repository';

export const save = ({ dispatch }, id, form) => {
  dispatch('CLEAR_MASKS_USER');
  return UserRepository.save(id, form).then(response => {
    dispatch('CLEAR_USER_DATA');
    successSaveData('O usuário foi salvo com sucesso!');
    return response.json();
  });
};

export const findUser = ({ dispatch }, id) =>
  UserRepository.get(id).then((result) => {
    dispatch('FIND_USER', result);
  })
  .catch(() => errorLoadData());

export const loadDataTable = ({ dispatch }, query = {}) => {
  UserRepository.list(query)
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_USER', MODULE_API);
    dispatch('LOAD_DATA_USER', result);
  })
  .catch(() => errorLoadData());
};

export const disableUser = ({ dispatch }, id) => UserRepository.remove(id);

export const updateInput = ({ dispatch }, name, e) => {
  dispatch('UPDATE_INPUT_USER', name, e.target.value);
};
export const updateSearchState = ({ dispatch }, value) => {
  dispatch('UPDATE_SEARCH_USER', value);
};

export const clearUserData = ({ dispatch }) => {
  dispatch('CLEAR_USER_DATA');
};

export const paginate = ({ dispatch }, page) => {
  UserRepository.list({ page })
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_USER', MODULE_API);
    dispatch('LOAD_DATA_USER', result);
  })
  .catch(() => errorLoadData());
};
