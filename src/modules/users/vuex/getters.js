export const user = ({ user }) => user.logged;
export const users = ({ users }) => users;
export const data = ({ users }) => users.data;
export const search = ({ users }) => users.search;
export const tableData = ({ users }) => users.tableData;
export const urlLoadingData = ({ users }) => users.urlLoadingData;
export const alert = ({ alert }) => alert;
