import { clearString } from 'src/helpers/helper';

export default {
  CLEAR_USER_DATA({ users }) {
    users.data = {};
  },
  UPDATE_INPUT_USER({ users }, fieldname, value) {
    users.data[fieldname] = value;
  },
  UPDATE_SEARCH_USER({ users }, name) {
    users.search = name;
  },
  CLEAR_MASKS_USER({ users }) {
    users.data.telphone = clearString(users.data.telphone);
  },
  FIND_USER({ users }, result) {
    users.data = result;
  },
  LOAD_DATA_USER({ users }, result) {
    users.tableData = result;
  },
  SET_URL_LOAD_DATA_USER({ users }, url) {
    users.urlLoadingData = url;
  },
};
