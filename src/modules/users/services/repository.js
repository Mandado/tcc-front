import Vue from 'vue';
import VueResource from 'vue-resource';
import { API_URL } from 'src/config';

Vue.use(VueResource);

export const MODULE_API = `${API_URL}/api/users{/id}`;
const resource = Vue.resource(MODULE_API);

const create = (data) => resource.save(data);
const update = (id, data) => resource.update({ id }, data);

export default {
  save(id, data) {
    return (!id) ? create(data) : update(id, data);
  },
  list(query = {}) {
    return resource.query(query)
      .then((response) => response.json());
  },
  get(id) {
    return resource.get({ id })
      .then((response) => response.json());
  },
  remove(id) {
    return resource.remove({ id })
      .then((response) => response.json());
  },
};
