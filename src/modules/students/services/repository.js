import Vue from 'vue';
import VueResource from 'vue-resource';
import { API_URL } from 'src/config';


Vue.use(VueResource);

export const MODULE_API = `${API_URL}/api/students{/id}`;

const customActions = {
  evaluationStudent: { method: 'POST', url: `${MODULE_API}/evaluation` },
  getEvaluationStudent: { method: 'GET', url: `${MODULE_API}/evaluation` },
};

const resource = Vue.resource(MODULE_API, {}, customActions);

const create = (data) => resource.save(data);
const update = (id, data) => resource.update({ id }, data);

export default {
  save(id, data) {
    return (!id) ? create(data) : update(id, data);
  },
  list(query = {}) {
    return resource.query(query)
      .then((response) => response.json());
  },
  get(id) {
    return resource.get({ id })
      .then((response) => response.json());
  },
  evaluationStudent(id, data) {
    return resource.evaluationStudent({ id }, data)
      .then((response) => response.json());
  },
  getEvaluationStudent(id) {
    return resource.getEvaluationStudent({ id })
      .then((response) => response.json());
  },
};
