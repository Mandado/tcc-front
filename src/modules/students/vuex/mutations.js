import { clearString } from 'src/helpers/helper';
const formatResult = ({ id, name }) => ({ id, name });

export default {
  CLEAR_STUDENT_DATA({ students }) {
    students.data = {};
  },
  UPDATE_INPUT_STUDENT({ students }, fieldname, value) {
    students.data[fieldname] = value;
  },
  CLEAR_MASKS_STUDENT({ students }) {
    students.data.telphone = clearString(students.data.telphone);
  },
  UPDATE_SEARCH_STUDENT({ students }, name) {
    students.search = name;
  },
  FIND_STUDENT({ students }, result) {
    students.data = result;
  },
  LOAD_DATA_STUDENT({ students }, result) {
    students.tableData = result;
  },
  LOAD_STUDENT_LIST({ students }, result) {
    students.list = result.data.map(formatResult);
  },
  SET_URL_LOAD_DATA_STUDENT({ students }, url) {
    students.urlLoadingData = url;
  },
};
