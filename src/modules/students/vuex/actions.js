import { successSaveData, errorLoadData } from 'src/services/alert';

import StudentRepository, { MODULE_API } from 'modules/students/services/repository';

export const save = ({ dispatch }, id, form) => {
  dispatch('CLEAR_MASKS_STUDENT');
  return StudentRepository.save(id, form).then(response => {
    dispatch('CLEAR_STUDENT_DATA');
    successSaveData('O aluno foi salvo com sucesso!');
    return response.json();
  });
};

export const findStudent = ({ dispatch }, id) =>
  StudentRepository.get(id).then((result) => {
    dispatch('FIND_STUDENT', result);
  })
  .catch(() => errorLoadData());

export const updateInput = ({ dispatch }, name, e) => {
  dispatch('UPDATE_INPUT_STUDENT', name, e.target.value);
};

export const loadDataTable = ({ dispatch }, query = {}) => {
  StudentRepository.list(query)
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_STUDENT', MODULE_API);
    dispatch('LOAD_DATA_STUDENT', result);
  })
  .catch(() => errorLoadData());
};

export const loadStudentsList = ({ dispatch }, query = {}) =>
  StudentRepository.list(query)
  .then((result) => {
    dispatch('LOAD_STUDENT_LIST', result);
    return result;
  })
  .catch(() => errorLoadData());

export const updateSearchState = ({ dispatch }, value) => {
  dispatch('UPDATE_SEARCH_STUDENT', value);
};

export const clearStudentData = ({ dispatch }) => {
  dispatch('CLEAR_STUDENT_DATA');
};

export const paginate = ({ dispatch }, page) => {
  StudentRepository.list({ page })
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_STUDENT', MODULE_API);
    dispatch('LOAD_DATA_STUDENT', result);
  })
  .catch(() => errorLoadData());
};
