export const students = ({ students }) => students;
export const studentsList = ({ students }) => students.list;
export const data = ({ students }) => students.data;
export const search = ({ students }) => students.search;
export const tableData = ({ students }) => students.tableData;
export const urlLoadingData = ({ students }) => students.urlLoadingData;
export const alert = ({ alert }) => alert;
