import Vue from 'vue';
import VueResource from 'vue-resource';
import { API_URL } from 'src/config';

Vue.use(VueResource);

export const MODULE_API = `${API_URL}/api/reports`;

const customActions = {
  sessionsAtended: { method: 'GET', url: `${MODULE_API}/sessions-atended` },
  sessionsAtendedByStudent: { method: 'GET', url: `${MODULE_API}/sessions-atended-by-student` },
  patients: { method: 'GET', url: `${MODULE_API}/patients` },
  evaluationStudents: { method: 'GET', url: `${MODULE_API}/evaluation-students` },
};

const resource = Vue.resource(MODULE_API, {}, customActions);

export default {
  sessionsAtended(form) {
    return resource.sessionsAtended({ ...form })
    .then((response) => response.json());
  },
  sessionsAtendedByStudent(form) {
    return resource.sessionsAtendedByStudent({ ...form })
    .then((response) => response.json());
  },
  patients(form) {
    return resource.patients({ ...form })
    .then((response) => response.json());
  },
  evaluationStudents(form) {
    return resource.evaluationStudents({ ...form })
    .then((response) => response.json());
  },
};
