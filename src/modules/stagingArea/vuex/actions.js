import StagingAreaRepository, { MODULE_API } from '../services/repository';
import { successSaveData, errorLoadData } from 'src/services/alert';

export const save = ({ dispatch }, id, form) =>
  StagingAreaRepository.save(id, { ...form.data }).then(response => {
    dispatch('CLEAR_STAGING_DATA_FORM');
    successSaveData('A especialidade foi salva com sucesso!');
    return response.json();
  }).catch(error => errorLoadData(error.json().name[0]));

export const findStagingArea = ({ dispatch }, id) =>
  StagingAreaRepository.get(id).then((result) => {
    dispatch('FIND_STAGING_AREA_DATA', result);
  })
    .catch(() => errorLoadData());

export const updateName = ({ dispatch }, e) => {
  dispatch('UPDATE_NAME_STAGING_AREA', e.target.value);
};

export const loadDataTable = ({ dispatch }, query = {}) => {
  StagingAreaRepository.list(query)
    .then((result) => {
      dispatch('SET_URL_LOAD_DATA_STAGING_AREA', MODULE_API);
      dispatch('LOAD_DATA_STAGING_AREA_TABLE', result);
    })
    .catch(() => errorLoadData());
};

export const updateSearchState = ({ dispatch }, value) => {
  dispatch('UPDATE_SEARCH_STAGING_AREA', value);
};

export const clearStagingData = ({ dispatch }) => {
  dispatch('CLEAR_STAGING_DATA_FORM');
};

export const paginate = ({ dispatch }, page, q) => {
  const query = { page };

  if (q) {
    query.q = q;
  }

  StagingAreaRepository.list(query)
    .then((result) => {
      dispatch('SET_URL_LOAD_DATA_STAGING_AREA', MODULE_API);
      dispatch('LOAD_DATA_STAGING_AREA', result);
    })
    .catch(() => errorLoadData());
};
