export default {
  CLEAR_STAGING_DATA_FORM({ stagingArea }) {
    stagingArea.data = {};
  },
  UPDATE_NAME_STAGING_AREA({ stagingArea }, name) {
    stagingArea.data.name = name;
    stagingArea.data.name_validation = name;
  },
  UPDATE_SEARCH_STAGING_AREA({ stagingArea }, value) {
    stagingArea.search = value;
  },
  FIND_STAGING_AREA_DATA({ stagingArea }, result) {
    stagingArea.data = result;
  },
  LOAD_DATA_STAGING_AREA_TABLE({ stagingArea }, result) {
    stagingArea.tableData = result;
  },
  SET_URL_LOAD_DATA_STAGING_AREA({ stagingArea }, url) {
    stagingArea.urlLoadingData = url;
  },
};
