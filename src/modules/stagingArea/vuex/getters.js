export const stagingArea = ({ stagingArea }) => stagingArea;
export const data = ({ stagingArea }) => stagingArea.data;
export const search = ({ stagingArea }) => stagingArea.search;
export const tableData = ({ stagingArea }) => stagingArea.tableData;
export const urlLoadingData = ({ stagingArea }) => stagingArea.urlLoadingData;
export const alert = ({ alert }) => alert;
