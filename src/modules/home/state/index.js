export const homeMutations = {
  INCREMENT(state, total) {
    state.counter += total;
  },
  updateMessage(state, message) {
    state.message = message;
  },
};

export const homeState = {
  counter: 0,
  message: 'ok',
};
