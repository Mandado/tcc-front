export const increment = ({ dispatch }, total = 1) => dispatch('INCREMENT', total);
export const updateMessage = ({ dispatch }, e) => dispatch('updateMessage', e.target.value);
