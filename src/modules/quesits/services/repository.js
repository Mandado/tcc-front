import Vue from 'vue';
import VueResource from 'vue-resource';
import { API_URL } from 'src/config';

Vue.use(VueResource);

export const MODULE_API = `${API_URL}/api/quesits{/id}`;

const customActions = {
  listActives: { method: 'GET', url: `${API_URL}/api/quesits/actives` },
};

const resource = Vue.resource(MODULE_API, {}, customActions);

const create = (data) => resource.save(data);
const update = (id, data) => resource.update({ id }, data);

export default {
  save(id, data) {
    return (!id) ? create(data) : update(id, data);
  },
  list(query = {}) {
    return resource.query(query)
      .then((response) => response.json());
  },
  get(id) {
    return resource.get({ id })
      .then((response) => response.json());
  },
  listActives() {
    return resource.listActives()
      .then((response) => response.json());
  },
  remove(id) {
    return resource.remove({ id })
      .then((response) => response.json());
  },
};
