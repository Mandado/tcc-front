export default {
  CLEAR_QUESIT_DATA_FORM({ quesit }) {
    quesit.data = {
      description: '',
      minNote: 0,
      maxNote: 0,
      active: true,
    };
  },
  UPDATE_INPUT_QUESIT({ quesit }, name, valueInput) {
    const value = (name === 'enabled') ? JSON.parse(valueInput) : valueInput;
    quesit.data[name] = value;
  },
  UPDATE_SEARCH_QUESIT({ quesit }, value) {
    quesit.search = value;
  },
  FIND_QUESIT_DATA({ quesit }, result) {
    quesit.data = result;
  },
  LOAD_DATA_QUESIT_TABLE({ quesit }, result) {
    quesit.tableData = result;
  },
  SET_URL_LOAD_DATA_QUESIT({ quesit }, url) {
    quesit.urlLoadingData = url;
  },
};
