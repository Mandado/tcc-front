export const quesit = ({ quesit }) => quesit;
export const data = ({ quesit }) => quesit.data;
export const search = ({ quesit }) => quesit.search;
export const tableData = ({ quesit }) => quesit.tableData;
export const urlLoadingData = ({ quesit }) => quesit.urlLoadingData;
export const alert = ({ alert }) => alert;
