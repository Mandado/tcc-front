export default {
  quesit: {
    data: {
      description: '',
      minNote: 0,
      maxNote: 0,
      active: true,
    },
    tableData: {
      total: 0,
      per_page: 15,
      current_page: 1,
      last_page: 0,
      next_page_url: null,
      prev_page_url: null,
      from: null,
      to: null,
      data: [],
    },
    urlLoadingData: '',
    search: '',
  },
};
