import QuesitRepository, { MODULE_API } from '../services/repository';
import { successSaveData, errorLoadData } from 'src/services/alert';

export const save = ({ dispatch }, id, data) =>
QuesitRepository.save(id, data).then(response => {
  dispatch('CLEAR_QUESIT_DATA_FORM');
  successSaveData('O quesito foi salvo com sucesso!');
  return response.json();
});

export const findQuesit = ({ dispatch }, id) =>
QuesitRepository.get(id).then((result) => {
  dispatch('FIND_QUESIT_DATA', result);
})
.catch(() => errorLoadData());

export const updateInput = ({ dispatch }, name, e) => {
  dispatch('UPDATE_INPUT_QUESIT', name, e.target.value);
};

export const loadDataTable = ({ dispatch }, query = {}) => {
  QuesitRepository.list(query)
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_QUESIT', MODULE_API);
    dispatch('LOAD_DATA_QUESIT_TABLE', result);
  })
  .catch(() => errorLoadData());
};

export const updateSearchState = ({ dispatch }, value) => {
  dispatch('UPDATE_SEARCH_QUESIT', value);
};

export const clearQuesitData = ({ dispatch }) => {
  dispatch('CLEAR_QUESIT_DATA_FORM');
};

export const disableQuesit = ({ dispatch }, id) => QuesitRepository.remove(id);

export const paginate = ({ dispatch }, page, q) => {
  const query = { page };

  if (q) {
    query.q = q;
  }

  QuesitRepository.list(query)
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_QUESIT', MODULE_API);
    dispatch('LOAD_DATA_QUESIT', result);
  })
  .catch(() => errorLoadData());
};
