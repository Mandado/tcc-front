import Vue from 'vue';
import VueResource from 'vue-resource';
import { API_URL } from 'src/config';

Vue.use(VueResource);

export const MODULE_API = `${API_URL}/api/waiting-list{/id}`;
const resource = Vue.resource(MODULE_API);
const create = (data) => resource.save(data);

export default {
  save(data) {
    return create(data);
  },
  list(query = {}) {
    return resource.query(query)
      .then((response) => response.json());
  },
  get(id) {
    return resource.get({ id })
      .then((response) => response.json());
  },
  remove(id) {
    return resource.delete({ id })
      .then((response) => response.json());
  },
};
