export const waitingList = ({ waitingList }) => waitingList;
export const data = ({ waitingList }) => waitingList.data;
export const search = ({ waitingList }) => waitingList.search;
export const tableData = ({ waitingList }) => waitingList.tableData;
export const urlLoadingData = ({ waitingList }) => waitingList.urlLoadingData;
export const alert = ({ alert }) => alert;
