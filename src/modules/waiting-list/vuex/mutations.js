export default {
  CLEAR_WAITING_DATA({ waitingList }) {
    waitingList.data = {};
  },
  UPDATE_INPUT_WAITING({ waitingList }, fieldname, value) {
    waitingList.data[fieldname] = value;
  },
  UPDATE_SEARCH_WAITING({ waitingList }, name) {
    waitingList.search = name;
  },
  FIND_WAITING({ waitingList }, result) {
    waitingList.data = result;
  },
  LOAD_DATA_WAITING({ waitingList }, result) {
    waitingList.tableData = result;
  },
  SET_URL_LOAD_DATA_WAITING({ waitingList }, url) {
    waitingList.urlLoadingData = url;
  },
};
