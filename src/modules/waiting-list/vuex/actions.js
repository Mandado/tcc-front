import { successSaveData, errorLoadData } from 'src/services/alert';

import WaitingListRepository, { MODULE_API } from 'modules/waiting-list/services/repository';

export const save = ({ dispatch }, idPatientSelected, idStagingAreaSelected) =>
  WaitingListRepository.save({
    stagingArea: idStagingAreaSelected,
    patient: idPatientSelected,
  }).then(response => {
    dispatch('CLEAR_WAITING_DATA');
    successSaveData('O paciente foi cadastrado na lista de espera com sucesso!');
    return response.json();
  });

export const loadDataTable = ({ dispatch }, query = {}) => {
  WaitingListRepository.list(query)
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_WAITING', MODULE_API);
    dispatch('LOAD_DATA_WAITING', result);
  })
  .catch(() => errorLoadData());
};

export const pullPatientOfList = ({ dispatch }, patientId) =>
  WaitingListRepository.remove(patientId);

export const updateSearchState = ({ dispatch }, value) => {
  dispatch('UPDATE_SEARCH_WAITING', value);
};

export const clearUserData = ({ dispatch }) => {
  dispatch('CLEAR_WAITING_DATA');
};

export const paginate = ({ dispatch }, page) => {
  WaitingListRepository.list({ page })
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_WAITING', MODULE_API);
    dispatch('LOAD_DATA_WAITING', result);
  })
  .catch(() => errorLoadData());
};
