import Vue from 'vue';
import VueResource from 'vue-resource';
import { API_URL } from 'src/config';

export const MODULE_API = `${API_URL}/api/users{/id}`;

const customActions = {
  authenticate: { method: 'POST', url: `${API_URL}/api/users/authenticate` },
  isLogged: { method: 'GET', url: `${API_URL}/api/users/authenticated` },
};

Vue.use(VueResource);

const resource = Vue.resource(MODULE_API, {}, customActions);

const create = (data) => resource.save(data);
const update = (id, data) => resource.update({ id }, data);


export default {
  save(id, data) {
    return (!id) ? create(data) : update(id, data);
  },
  list(query = {}) {
    return resource.query(query)
    .then((response) => response.json());
  },
  get(id) {
    return resource.get({ id })
    .then((response) => response.json());
  },
  authenticate(login, password) {
    return resource.authenticate({ login, password })
    .then((response) => response.json());
  },
  logout() {
    return new Promise((resolve) => {
      localStorage.removeItem('id_token');
      resolve();
    });
  },
  isLogged() {
    return resource.isLogged()
    .then((response) => response.json());
  },
  setToken(token) {
    localStorage.setItem('id_token', token);
  },
};
