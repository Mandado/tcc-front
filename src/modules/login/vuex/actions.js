import LoginRepository from '../services/repository';
import { errorAlert } from 'src/services/alert';

export const authenticate = ({ dispatch }, login, password, keepLogged) =>
  LoginRepository.authenticate(login, password, keepLogged).then(response => {
    const token = response.token;
    dispatch('SET_TOKEN', token);
    LoginRepository.setToken(token);
    return response;
  }).catch(error => {
    if (error) {
      errorAlert(error.json().msg);
    }
  });

export const logout = () => LoginRepository.logout();

export const setDataUserLogged = ({ dispatch }) => {
  LoginRepository.isLogged().then(user => {
    dispatch('SET_USER_DATA_LOGGED', user);
  });
};

export const updateInput = ({ dispatch }, name, event) => {
  const value = (name === 'remember') ? event.target.checked : event.target.value;

  dispatch('UPDATE_INPUT_LOGIN', name, value);
};
