export const user = ({ user }) => user;
export const data = ({ user }) => user.data;
export const dataLogged = ({ user }) => user.logged;
