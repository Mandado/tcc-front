export default {
  CLEAR_LOGIN_DATA({ user }) {
    user.data = {};
  },
  UPDATE_INPUT_LOGIN({ user }, name, value) {
    user.data[name] = value;
  },
  SET_USER_DATA_LOGGED({ user }, data) {
    user.logged = data;
  },
  SET_TOKEN({ user }, token) {
    user.logged.token = token;
  },
};
