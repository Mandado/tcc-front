import { successSaveData, errorLoadData } from 'src/services/alert';
// import { clearString } from 'src/helpers/helper';
import StagingAreaRepository from 'modules/stagingArea/services/repository';
import PatientsRepository, { MODULE_API } from 'modules/patients/services/repository';

export const save = ({ dispatch }, id, form) => {
  dispatch('SET_ACTIVE_PATIENT', form);
  dispatch('CLEAR_MASKS_PATIENT');
  return PatientsRepository.save(id, form).then(response => {
    dispatch('CLEAR_PATIENTS_DATA');
    successSaveData('O paciente foi salvo com sucesso!');
    return response.json();
  });
};

export const findPatient = ({ dispatch }, id) =>
  PatientsRepository.get(id).then((result) => {
    dispatch('FIND_PATIENTS', result);
  })
  .catch(() => errorLoadData());

export const updateInput = ({ dispatch }, name, e) => {
  dispatch('UPDATE_INPUT_PATIENT', name, e.target.value);
};

export const loadStagingAreasSelect = ({ dispatch }) =>
  StagingAreaRepository.list()
  .then((result) => {
    dispatch('LOAD_DATA_STAGING_AREA_TABLE', result);

    return result;
  })
  .catch(() => errorLoadData());

export const loadDataTable = ({ dispatch }, query = {}) => {
  PatientsRepository.list(query)
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_PATIENTS', MODULE_API);
    dispatch('LOAD_DATA_PATIENTS', result);
  })
  .catch(() => errorLoadData());
};

export const updateSearchState = ({ dispatch }, value) => {
  dispatch('UPDATE_SEARCH_PATIENTS', value);
};

export const clearPatientsData = ({ dispatch }) => {
  dispatch('CLEAR_PATIENTS_DATA');
};

export const paginate = ({ dispatch }, page) => {
  PatientsRepository.list({ page })
  .then((result) => {
    dispatch('SET_URL_LOAD_DATA_PATIENTS', MODULE_API);
    dispatch('LOAD_DATA_PATIENTS', result);
  })
  .catch(() => errorLoadData());
};
