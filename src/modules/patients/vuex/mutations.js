import { setActive } from 'modules/patients/services/repository';
import { clearString } from 'src/helpers/helper';
import { maskValueTelphone, maskValueCpf } from 'masks';

export default {
  CLEAR_PATIENTS_DATA({ patients }) {
    patients.data = {
      name: '',
      age: '',
      cpf: '',
      sex: '',
      race: '',
      diagnostic: '',
      ocupation: '',
      active: '',
      street: '',
      number: '',
      neighborhood: '',
      city: '',
      state: '',
      telphone: '',
      responsible: '',
      academicResponsible: '',
    };
  },
  UPDATE_INPUT_PATIENT({ patients }, fieldname, value) {
    patients.data[fieldname] = value;
  },
  CLEAR_MASKS_PATIENT({ patients }) {
    patients.data.telphone = clearString(patients.data.telphone);
    patients.data.cpf = clearString(patients.data.cpf);
  },
  SET_ACTIVE_PATIENT({ patients }, form) {
    patients.data.active = setActive(form.active);
  },
  UPDATE_SEARCH_PATIENTS({ patients }, name) {
    patients.search = name;
  },
  FIND_PATIENTS({ patients }, result) {
    result.active = (result.active) ? 'S' : 'N';
    result.sex = result.sex.replace(/\s+/, '');
    result.stagingArea = result.staging_area_id;
    result.telphone = maskValueTelphone(result.telphone);
    result.cpf = maskValueCpf(result.cpf);
    patients.data = result;
  },
  LOAD_DATA_PATIENTS({ patients }, result) {
    patients.tableData = result;
  },
  SET_URL_LOAD_DATA_PATIENTS({ patients }, url) {
    patients.urlLoadingData = url;
  },
};
