export const patients = ({ patients }) => patients;
export const data = ({ patients }) => patients.data;
export const search = ({ patients }) => patients.search;
export const tableData = ({ patients }) => patients.tableData;
export const urlLoadingData = ({ patients }) => patients.urlLoadingData;
export const alert = ({ alert }) => alert;
