import Vue from 'vue';
import VueResource from 'vue-resource';
import { API_URL } from 'src/config';


const customActions = {
  patientsName: { method: 'GET', url: `${API_URL}/api/patients/names` },
  importSessions: { method: 'POST', url: `${API_URL}/api/patients{/id}/import-sessions` },
  patientEvolution: { method: 'POST', url: `${API_URL}/api/patients{/id}/evolution` },
  removeEvolution: { method: 'DELETE', url: `${API_URL}/api/patients{/id}/evolution` },
  patientEvolutions: { method: 'GET', url: `${API_URL}/api/patients{/id}/evolutions` },
};


Vue.use(VueResource);

export const MODULE_API = `${API_URL}/api/patients{/id}`;
const resource = Vue.resource(MODULE_API, {}, customActions);

const create = (data) => resource.save(data);
const update = (id, data) => resource.update({ id }, data);

export default {
  save(id, data) {
    return (!id) ? create(data) : update(id, data);
  },
  importSessions(id, data) {
    return resource.importSessions({ id }, data)
      .then((response) => response.json());
  },
  patientEvolution(id, data) {
    return resource.patientEvolution({ id }, data)
      .then((response) => response.json());
  },
  removeEvolution(id) {
    return resource.removeEvolution({ id })
      .then((response) => response.json());
  },
  patientEvolutions(id) {
    return resource.patientEvolutions({ id })
      .then((response) => response.json());
  },
  list(query = {}) {
    return resource.query(query)
      .then((response) => response.json());
  },
  patientsNames() {
    return resource.patientsName()
      .then((response) => response.json());
  },
  get(id) {
    return resource.get({ id })
      .then((response) => response.json());
  },
};

export const setActive = (value) => value === 'S';
